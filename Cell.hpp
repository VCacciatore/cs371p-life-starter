#ifndef CELL_H
#define CELL_H

#include "AbstractCell.hpp"

class Cell : public AbstractCell {
	private:
		AbstractCell* _p;

	public:
		Cell (AbstractCell* p) : _p (p) {}

		// how do you copy Cells

		~Cell () {
			delete _p;
        }
};

#endif