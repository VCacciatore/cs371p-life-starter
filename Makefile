.DEFAULT_GOAL := all
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen

ifeq ($(shell uname -s), Life)
				BOOST    := /usr/local/include/boost
				CXX      := g++-11
				CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
				GCOV     := gcov-11
				GTEST    := /usr/local/src/googletest-master
				LDFLAGS  := -lgtest -lgtest_main
				LIB      := /usr/local/lib
				VALGRIND :=
else ifeq ($(shell uname -p), unknown)
				BOOST    := /usr/include/boost
				CXX      := g++
				CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
				GCOV     := gcov
				GTEST    := /usr/src/gtest
				LDFLAGS  := -lgtest -lgtest_main -pthread
				LIB      := /usr/lib
				VALGRIND := valgrind
else
				BOOST    := /usr/include/boost
				CXX      := g++-11
				CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
				GCOV     := gcov-11
				GTEST    := /usr/local/include/gtest
				LDFLAGS  := -lgtest -lgtest_main -pthread
				LIB      := /usr/local/lib
				VALGRIND := valgrind-3.17
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Life.log:
	git log > Life.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Life code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Life code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Life.cpp
	git add Life.hpp
	git add Cell.cpp
	git add Cell.hpp
	git add AbstractCell.cpp
	git add AbstractCell.hpp
	git add ConwayCell.cpp
	git add ConwayCell.hpp
	git add FredkinCell.cpp
	git add FredkinCell.hpp
	git add RunLifeCell.cpp
	git add RunLifeConway.cpp
	git add RunLifeFredkin.cpp
	-git add Life.log
	-git add html
	git add Makefile
	git add README.md
	git add RunLife.ctd
	git add TestLife.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunLifeCell: Life.hpp Life.cpp Cell.cpp Cell.hpp AbstractCell.hpp FredkinCell.hpp FredkinCell.cpp ConwayCell.hpp ConwayCell.cpp RunLifeCell.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) RunLifeCell.cpp
	$(CXX) $(CXXFLAGS) Life.hpp Life.cpp Cell.cpp Cell.hpp AbstractCell.hpp FredkinCell.hpp FredkinCell.cpp ConwayCell.hpp ConwayCell.cpp RunLifeCell.cpp -o RunLifeCell

RunLifeConway: Life.hpp Life.cpp AbstractCell.hpp ConwayCell.hpp ConwayCell.cpp RunLifeConway.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) RunLifeConway.cpp
	$(CXX) $(CXXFLAGS) Life.hpp Life.cpp AbstractCell.hpp ConwayCell.hpp ConwayCell.cpp RunLifeConway.cpp -o RunLifeConway

RunLifeFredkin: Life.hpp Life.cpp AbstractCell.hpp FredkinCell.hpp FredkinCell.cpp RunLifeFredkin.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) RunLifeFredkin.cpp
	$(CXX) $(CXXFLAGS) Life.hpp Life.cpp AbstractCell.hpp FredkinCell.hpp FredkinCell.cpp RunLifeFredkin.cpp -o RunLifeFredkin

# compile test harness
TestLife: Life.hpp Life.cpp TestLife.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) TestLife.cpp
	$(CXX) $(CXXFLAGS) Life.cpp TestLife.cpp -o TestLife $(LDFLAGS)

# run/test files, compile with make all
FILES :=                               \
    RunLifeCell                        \
    RunLifeConway                      \
    RunLifeFredkin                     \
    TestLife;

# compile all
all: $(FILES)

# execute test harness
test: TestLife
	-$(VALGRIND) ./TestLife
	$(GCOV) TestLife-Life.cpp | grep -B 2 "cpp.gcov"

# clone the Life test repo
../cs371p-life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-life-tests.git ../cs371p-life-tests

# test files in the Life test repo
T_FILES := `ls ../cs371p-life-tests/*.in`

# check integrity of all the test files in the Life test repo
ctd-check: ../cs371p-life-tests
	-for v in $(T_FILES); do echo $(CHECKTESTDATA) RunLife.ctd $$v; $(CHECKTESTDATA) RunLife.ctd $$v; done

# generate a random input file
ctd-generate:
	-$(CHECKTESTDATA) -g RunLife.ctd >> RunLife.gen;

# execute the run harness against a test file in the Life test repo and diff with the expected output
../cs371p-life-tests/%RunLifeConway.in: RunLifeConway
	$(CHECKTESTDATA) RunLife.ctd $@.in
	./RunLifeConway < $@.in > RunLife.tmp
	diff RunLife.tmp $@.ans

../cs371p-life-tests/%RunLifeFredkin.in: RunLifeFredkin
	$(CHECKTESTDATA) RunLife.ctd $@.in
	./RunLifeFredkin < $@.in > RunLife.tmp
	diff RunLife.tmp $@.ans

../cs371p-life-tests/%RunLifeCell.in: RunLifeCell
	$(CHECKTESTDATA) RunLife.ctd $@.in
	./RunLifeCell < $@.in > RunLife.tmp
	diff RunLife.tmp $@.ans

# execute the run harness against all of the test files in the Life test repo and diff with the expected output
run: ../cs371p-life-tests
	-for v in $(T_FILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Life.cpp
	$(ASTYLE) Life.hpp
	$(ASTYLE) Cell.cpp
	$(ASTYLE) Cell.hpp
	$(ASTYLE) AbstractCell.cpp
	$(ASTYLE) AbstractCell.hpp
	$(ASTYLE) ConwayCell.cpp
	$(ASTYLE) ConwayCell.hpp
	$(ASTYLE) FredkinCell.cpp
	$(ASTYLE) FredkinCell.hpp
	$(ASTYLE) RunLifeCell.cpp
	$(ASTYLE) RunLifeConway.cpp
	$(ASTYLE) RunLifeFredkin.cpp
	$(ASTYLE) TestLife.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=         \
    .gitignore     \
    .gitlab-ci.yml \
    Life.log       \
    html;

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.gen
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLifeCell
	rm -f RunLifeConway
	rm -f RunLifeFredkin
	rm -f TestLife

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo  'shell uname -p'
	@echo $(shell uname -p)

	@echo
	@echo  'shell uname -s'
	@echo $(shell uname -s)

	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version

	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version

	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version

	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version

	@echo
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version

	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version

	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version

	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version

	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version

ifneq ($(shell uname -s), Life)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif

	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version

	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	@echo "pkg-config --modversion gtest"
	@pkg-config --modversion gtest
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
